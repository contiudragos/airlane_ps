package com.airline.controller;

import com.airline.business_logic.FlightBusinessLogic;
import com.airline.model.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/flight")
public class FlightController {

    private final FlightBusinessLogic businessLogic;

    @Autowired
    public FlightController(FlightBusinessLogic business) {
        this.businessLogic = business;
    }

    @GetMapping
    public ResponseEntity<List<Flight>> getFlights() {
        return new ResponseEntity<>(businessLogic.getFlights(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> addFlight(@RequestBody Flight flight) {
        try {
            businessLogic.addFlight(flight);

            return new ResponseEntity<>("Flight successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity<String> updateFlight(@RequestBody Flight flight) {
        try {
            businessLogic.updateFlight(flight);

            return new ResponseEntity<>("Flight successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    @RequestMapping(value = "/{id}")
    public ResponseEntity<String> deleteAirport(@PathVariable Long id) {
        try {
            businessLogic.deleteFlight(id);

            return new ResponseEntity<>("Flight successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
