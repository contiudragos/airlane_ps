package com.airline.controller;

import com.airline.business_logic.EmployeeBusinessLogic;
import com.airline.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/employee")
public class EmployeeController {

    private final EmployeeBusinessLogic businessLogic;

    @Autowired
    public EmployeeController(EmployeeBusinessLogic businessLogic) {
        this.businessLogic = businessLogic;
    }

    @GetMapping
    public ResponseEntity<List<Object>> getStudents() {
        return new ResponseEntity<>(businessLogic.getAllEmployees(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> addEmployee(@RequestBody Employee employee) {
        try {
            businessLogic.addEmployee(employee);

            return new ResponseEntity<>("Employee successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity<String> updateEmployee(@RequestBody Employee employee) {
        try {
            businessLogic.updateEmployee(employee);

            return new ResponseEntity<>("Employee successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
