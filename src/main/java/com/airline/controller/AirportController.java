package com.airline.controller;

import com.airline.business_logic.AirportBusinessLogic;
import com.airline.model.Airport;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/airport")
public class AirportController {

    private final AirportBusinessLogic businessLogic;

    @Autowired
    public AirportController(AirportBusinessLogic businessLogic) {
        this.businessLogic = businessLogic;
    }

    @GetMapping
    public ResponseEntity<List<Airport>> getAirports() {
        return new ResponseEntity<>(this.businessLogic.getAirports(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> addAirport(@RequestBody Airport airport) {
        try {
            businessLogic.addAirport(airport);

            return new ResponseEntity<>("Airport successfully added", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity<String> updateAirport(@RequestBody Airport airport) {
        try {
            businessLogic.updateAirport(airport);

            return new ResponseEntity<>("Airport successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    @RequestMapping(value = "/{id}")
    public ResponseEntity<String> deleteAirport(@PathVariable Long id) {
        try {
            businessLogic.deleteAirport(id);
            return new ResponseEntity<>("Airport successfully deleted", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
