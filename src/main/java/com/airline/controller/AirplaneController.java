package com.airline.controller;

import com.airline.business_logic.AirplaneBusinessLogic;
import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/airplane")
public class AirplaneController {

    private final AirplaneBusinessLogic businessLogic;

    @Autowired
    public AirplaneController(AirplaneBusinessLogic businessLogic) {
        this.businessLogic = businessLogic;
    }

    @GetMapping
    public ResponseEntity<List<Airplane>> getAirplanes() {
        return new ResponseEntity<>(businessLogic.getAirplanes(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> addAirplane(@RequestBody Airplane airplane) {
        try {
            businessLogic.addAirplane(airplane);

            return new ResponseEntity<>("Airplane successfully added", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity<String> updateAirplane(@RequestBody Airplane airplane) {
        try {
            businessLogic.updateAirplane(airplane);

            return new ResponseEntity<>("Airplane successfully updated", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    @RequestMapping(value = "/{id}")
    public ResponseEntity<String> deleteAirplane(@PathVariable Long id) {
        try {
            businessLogic.deleteAirplane(id);

            return new ResponseEntity<>("Airplane successfully deleted", HttpStatus.OK);
        }catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
