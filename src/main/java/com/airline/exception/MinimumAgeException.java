package com.airline.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MinimumAgeException extends RuntimeException {

    public MinimumAgeException(String message) {
        super(message);
    }
}
