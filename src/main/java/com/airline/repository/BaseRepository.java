package com.airline.repository;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BaseRepository<T> {
    void add(T object);
    List<T> getAll();
    void delete(Long id);
    void update(T object);
    Optional<T> findById(Long id);
    List<T> findByCriteria(String argument, String value);
}
