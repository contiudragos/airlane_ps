package com.airline.repository.JPA;

import com.airline.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FlightRepositoryJPA extends JpaRepository<Flight, Long> {

    @Query(value = "SELECT * FROM flight WHERE flight.arrival = ?1", nativeQuery = true)
    List<Flight> findByArrival(String value);

    @Query(value = "SELECT * FROM flight WHERE flight.departure = ?1", nativeQuery = true)
    List<Flight> findByDeparture(String value);

    @Query(value = "SELECT * FROM flight WHERE flight.date = ?1", nativeQuery = true)
    List<Flight> findByDate(LocalDate value);

    @Query(value = "SELECT * FROM flight WHERE flight.flight_number = ?1", nativeQuery = true)
    List<Flight> findByFlightNumber(String value);

    @Query(value = "SELECT * FROM flight WHERE flight.plane_unique_id = ?1", nativeQuery = true)
    List<Flight> findByPlaneUid(String value);
}
