package com.airline.repository.JPA;

import com.airline.model.Airplane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirplaneRepositoryJPA extends JpaRepository<Airplane, Long>{

    @Query(value = "SELECT * FROM airplane WHERE airplane.capacity = ?1", nativeQuery = true)
    List<Airplane> findByCapacity(Long value);

    @Query(value = "SELECT * FROM airplane WHERE airplane.unique_id = ?1", nativeQuery = true)
    List<Airplane> findByUniqueId(String value);

    @Query(value = "SELECT * FROM airplane WHERE airplane.model = ?1", nativeQuery = true)
    List<Airplane> findByModel(String value);

    @Query(value = "SELECT * FROM airplane WHERE airplane.productionDate = ?1", nativeQuery = true)
    List<Airplane> findByProductionDate(String value);
}
