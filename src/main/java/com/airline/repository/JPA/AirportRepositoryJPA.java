package com.airline.repository.JPA;

import com.airline.model.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepositoryJPA extends JpaRepository<Airport, Long> {

    @Query(value = "SELECT * FROM airport WHERE airport.has_parking = ?1", nativeQuery = true)
    List<Airport> findByParking(int value);

    @Query(value = "SELECT * FROM airport WHERE airport.is_available= ?1", nativeQuery = true)
    List<Airport> findByAvailability(int value);

    @Query(value = "SELECT * FROM airport WHERE airport.location = ?1", nativeQuery = true)
    List<Airport> findByLocation(String value);

    @Query(value = "SELECT * FROM airport WHERE airport.name = ?1", nativeQuery = true)
    List<Airport> findByName(String value);
}
