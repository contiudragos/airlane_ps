package com.airline.repository.JPA;

import com.airline.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepositoryJPA extends JpaRepository<Employee, Long> {
}
