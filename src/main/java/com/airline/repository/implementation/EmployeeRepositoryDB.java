package com.airline.repository.implementation;

import com.airline.model.Airplane;
import com.airline.model.Employee;
import com.airline.repository.BaseRepository;
import com.airline.repository.JPA.EmployeeRepositoryJPA;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class EmployeeRepositoryDB implements BaseRepository<Employee> {
    private final EmployeeRepositoryJPA employeeRepositoryJPA;

    public EmployeeRepositoryDB(EmployeeRepositoryJPA employeeRepositoryJPA) {
        this.employeeRepositoryJPA = employeeRepositoryJPA;
    }

    @Override
    public void add(Employee employee) {
        this.employeeRepositoryJPA.save(employee);
    }

    @Override
    public List<Employee> getAll() {
        return this.employeeRepositoryJPA.findAll();
    }

    @Override
    public void delete(Long id) {
        this.employeeRepositoryJPA.deleteById(id);
    }

    @Override
    public void update(Employee employee) {
        this.employeeRepositoryJPA.save( employee);
    }

    @Override
    public Optional<Employee> findById(Long id) {
        return this.employeeRepositoryJPA.findById(id);
    }

    @Override
    public List<Employee> findByCriteria(String argument, String value) {
        return new ArrayList<>();
    }
}
