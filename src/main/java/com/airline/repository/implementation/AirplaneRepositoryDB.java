package com.airline.repository.implementation;

import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.airline.repository.BaseRepository;
import com.airline.repository.JPA.AirplaneRepositoryJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AirplaneRepositoryDB implements BaseRepository<Airplane> {

    private final AirplaneRepositoryJPA airplaneRepositoryJPA;

    @Autowired
    public AirplaneRepositoryDB(AirplaneRepositoryJPA repository) {
        this.airplaneRepositoryJPA = repository;
    }

    @Override
    public void add(Airplane airplane) {
        this.airplaneRepositoryJPA.save(airplane);
    }

    @Override
    public List<Airplane> getAll() {
        return this.airplaneRepositoryJPA.findAll();
    }

    @Override
    public void delete(Long id) {
        this.airplaneRepositoryJPA.deleteById(id);
    }

    @Override
    public void update(Airplane airplane) {

    }

    @Override
    public Optional<Airplane> findById(Long id) {
        return this.airplaneRepositoryJPA.findById(id);
    }

    @Override
    public List<Airplane> findByCriteria(String argument, String value) {
        return switch (argument) {
            case "unique_id" -> this.airplaneRepositoryJPA.findByUniqueId(value);
            case "capacity" -> this.airplaneRepositoryJPA.findByCapacity(Long.valueOf(value));
            case "model" -> this.airplaneRepositoryJPA.findByModel(value);
            case "production_date" -> this.airplaneRepositoryJPA.findByProductionDate(value);

            default -> throw new ConflictException("Invalid argument sent");
        };
    }
}
