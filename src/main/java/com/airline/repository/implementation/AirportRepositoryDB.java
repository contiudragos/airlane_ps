package com.airline.repository.implementation;

import com.airline.exception.ConflictException;
import com.airline.model.Airport;
import com.airline.repository.BaseRepository;
import com.airline.repository.JPA.AirportRepositoryJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AirportRepositoryDB implements BaseRepository<Airport> {
    private final AirportRepositoryJPA airportRepositoryJPA;

    @Autowired
    public AirportRepositoryDB(AirportRepositoryJPA airplaneRepositoryJPA) {
        this.airportRepositoryJPA = airplaneRepositoryJPA;
    }

    @Override
    public void add(Airport airport) {
        this.airportRepositoryJPA.save( airport);
    }

    @Override
    public List<Airport> getAll() {
        return this.airportRepositoryJPA.findAll();
    }

    @Override
    public void delete(Long id) {
        this.airportRepositoryJPA.deleteById(id);
    }

    @Override
    public void update(Airport airport) {
        this.airportRepositoryJPA.save( airport);
    }

    @Override
    public Optional<Airport> findById(Long id) {
        return this.airportRepositoryJPA.findById(id);
    }

    @Override
    public List<Airport> findByCriteria(String argument, String value) {
        return switch (argument) {
            case "has_parking" -> this.airportRepositoryJPA.findByParking(value.equals("true") ? 1 : 0);
            case "is_Available" -> this.airportRepositoryJPA.findByAvailability(value.equals("true") ? 1 : 0);
            case "location" -> this.airportRepositoryJPA.findByLocation(value);
            case "name" -> this.airportRepositoryJPA.findByName(value);

            default -> throw new ConflictException("Invalid argument sent");
        };
    }
}
