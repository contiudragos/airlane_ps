package com.airline.repository.implementation;

import com.airline.exception.ConflictException;
import com.airline.model.Flight;
import com.airline.repository.BaseRepository;
import com.airline.repository.JPA.FlightRepositoryJPA;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class FlightRepositoryDB implements BaseRepository<Flight> {
    private final FlightRepositoryJPA flightRepositoryJPA;

    public FlightRepositoryDB(FlightRepositoryJPA flightRepositoryJPA) {
        this.flightRepositoryJPA = flightRepositoryJPA;
    }

    @Override
    public void add(Flight flight) {
        this.flightRepositoryJPA.save( flight);
    }

    @Override
    public List<Flight> getAll() {
        return this.flightRepositoryJPA.findAll();
    }

    @Override
    public void delete(Long id) {
        this.flightRepositoryJPA.deleteById(id);
    }

    @Override
    public void update(Flight flight) {
        this.flightRepositoryJPA.save(flight);
    }

    @Override
    public Optional<Flight> findById(Long id) {
        return this.flightRepositoryJPA.findById(id);
    }

    @Override
    public List<Flight> findByCriteria(String argument, String value) {
        return switch (argument) {
            case "arrival" -> this.flightRepositoryJPA.findByArrival(value);
            case "departure" -> this.flightRepositoryJPA.findByDeparture(value);
            case "date" -> this.flightRepositoryJPA.findByDate(LocalDate.parse(value));
            case "flight_number" -> this.flightRepositoryJPA.findByFlightNumber(value);
            case "plane_unique_id" -> this.flightRepositoryJPA.findByPlaneUid(value);

            default -> throw new ConflictException("Invalid argument sent");
        };
    }
}
