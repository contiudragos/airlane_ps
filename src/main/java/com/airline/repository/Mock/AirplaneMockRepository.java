package com.airline.repository.Mock;

import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.airline.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class AirplaneMockRepository implements BaseRepository<Airplane> {
    private List<Airplane> list = new ArrayList<>();

    @Override
    public void add(Airplane airplane) {
        list.add( airplane);
    }

    @Override
    public List<Airplane> getAll() {
        return list;
    }

    @Override
    public void delete(Long id) {
        for(int i=0; i<list.size(); i++){
            if(Objects.equals(list.get(i).getId(), id)){
                list.remove(i);
                i--;
            }
        }
    }

    @Override
    public void update(Airplane airplane) {
        for (Airplane airplaneToUpdate : list) {
            if (Objects.equals(airplaneToUpdate.getId(), airplane.getId())) {
                airplaneToUpdate.setUniqueId(airplane.getUniqueId());
                airplaneToUpdate.setCapacity(airplane.getCapacity());
                airplaneToUpdate.setModel(airplane.getModel());
                airplaneToUpdate.setProductionDate(airplaneToUpdate.getProductionDate());
            }
        }
    }

    @Override
    public Optional<Airplane> findById(Long id) {
        for (Airplane airplane : list) {
            if (Objects.equals(airplane.getId(), id))
                return Optional.of(airplane);
        }

        return Optional.empty();
    }

    @Override
        public List<Airplane> findByCriteria(String argument, String value) {
        List<Airplane> airplanes = new ArrayList<>();
        switch (argument) {
            case "unique_id" -> {
                for (Airplane airplane : list) {
                    if (airplane.getUniqueId().equals(value))
                        airplanes.add(airplane);
                }
                return airplanes;
            }
            case "capacity" -> {
                for (Airplane airplane : list) {
                    if (airplane.getCapacity() == (Integer.parseInt(value)))
                        airplanes.add(airplane);
                }
                return airplanes;
            }
            case "model" -> {
                for (Airplane airplane : list) {
                    if (airplane.getModel().equals(value))
                        airplanes.add(airplane);
                }
                return airplanes;
            }
            case "production_date" -> {
                for (Airplane airplane : list) {
                    if (airplane.getProductionDate().equals(LocalDate.parse(value)))
                        airplanes.add(airplane);
                }
                return airplanes;
            }
            default -> throw new ConflictException("Invalid argument sent");
        }
    }
}
