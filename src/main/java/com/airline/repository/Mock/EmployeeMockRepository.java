package com.airline.repository.Mock;

import com.airline.model.Airplane;
import com.airline.model.Employee;
import com.airline.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class EmployeeMockRepository implements BaseRepository<Employee> {
    List<Employee> list = new ArrayList<>();

    @Override
    public void add(Employee employee) {
        list.add(employee);
    }

    @Override
    public List<Employee> getAll() {
        return list;
    }

    @Override
    public void delete(Long id) {
        for(int i=0; i<list.size(); i++){
            if(Objects.equals(list.get(i).getId(), id)){
                list.remove(i);
                i--;
            }
        }
    }

    @Override
    public void update(Employee employee) {

    }

    @Override
    public Optional<Employee> findById(Long id) {
        for (Employee employee : list) {
            if (Objects.equals(employee.getId(), id))
                return Optional.of(employee);
        }

        return Optional.empty();
    }

    @Override
    public List<Employee> findByCriteria(String argument, String value) {
        return new ArrayList<>();
    }
}
