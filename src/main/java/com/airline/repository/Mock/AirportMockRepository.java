package com.airline.repository.Mock;

import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.airline.model.Airport;
import com.airline.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class AirportMockRepository implements BaseRepository<Airport> {
    private List<Airport> list = new ArrayList<>();

    @Override
    public void add(Airport airport) {
        list.add(airport);
    }

    @Override
    public List<Airport> getAll() {
        return list;
    }

    @Override
    public void delete(Long id) {
        for (int i = 0; i < list.size(); i++) {
            if (Objects.equals(list.get(i).getId(), id)) {
                list.remove(i);
                i--;
            }
        }
    }

    @Override
    public void update(Airport airport) {
        for (Airport airportToBeUpdated : list) {
            if (Objects.equals(airportToBeUpdated.getId(), airport.getId())) {
                airportToBeUpdated.setHasParking(airport.getHasParking());
                airportToBeUpdated.setIsAvailable(airport.getIsAvailable());
                airportToBeUpdated.setLocation(airport.getLocation());
                airportToBeUpdated.setName(airport.getName());
            }
        }
    }

    @Override
    public Optional<Airport> findById(Long id) {
        for (Airport airport : list) {
            if (Objects.equals(airport.getId(), id))
                return Optional.of(airport);
        }

        return Optional.empty();
    }

    @Override
    public List<Airport> findByCriteria(String argument, String value) {
        List<Airport> airports = new ArrayList<>();
        switch (argument) {
            case "has_parking" -> {
                for (Airport airport : list) {
                    if (airport.getHasParking() == Integer.parseInt(value)) {
                        airports.add(airport);
                    }
                }

                return airports;
            }
            case "is_available" -> {
                for (Airport airport : list) {
                    if (airport.getIsAvailable() == Integer.parseInt(value)) {
                        airports.add(airport);
                    }
                }

                return airports;
            }
            case "location" -> {
                for (Airport airport : list) {
                    if (airport.getLocation().equals(value)) {
                        airports.add(airport);
                    }
                }

                return airports;
            }
            case "name" -> {
                for (Airport airport : list) {
                    if (airport.getName().equals(value)) {
                        airports.add(airport);
                    }
                }

                return airports;
            }
            default -> throw new ConflictException("Invalid argument sent");
        }
    }
}
