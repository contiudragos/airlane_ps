package com.airline.repository.Mock;

import com.airline.exception.ConflictException;
import com.airline.model.Flight;
import com.airline.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class FlightMockRepository implements BaseRepository<Flight> {
    private List<Flight> list = new ArrayList<>();

    @Override
    public void add(Flight flight) {
        list.add(flight);
    }

    @Override
    public List<Flight> getAll() {
        return list;
    }

    @Override
    public void delete(Long id) {
        for (int i = 0; i < list.size(); i++) {
            if (Objects.equals(list.get(i).getId(), id)) {
                list.remove(i);
                i--;
            }
        }
    }

    @Override
    public void update(Flight flight) {
        for (Flight flightToBeUpdated : list) {
            if (Objects.equals(flightToBeUpdated.getId(), flight.getId())) {
                flightToBeUpdated.setArrival(flight.getArrival());
                flightToBeUpdated.setFlightNumber(flight.getFlightNumber());
                flightToBeUpdated.setDate(flight.getDate());
                flightToBeUpdated.setDeparture(flight.getDeparture());
                flightToBeUpdated.setPlaneUniqueId(flight.getPlaneUniqueId());
            }
        }
    }

    @Override
    public Optional<Flight> findById(Long id) {
        for (Flight flight : list) {
            if (Objects.equals(flight.getId(), id))
                return Optional.of(flight);
        }

        return Optional.empty();
    }

    @Override
    public List<Flight> findByCriteria(String argument, String value) {
        List<Flight> flights = new ArrayList<>();

        switch (argument) {
            case "arrival" -> {
                for (Flight flight : list) {
                    if (flight.getArrival().equals(value))
                        flights.add(flight);
                }

                return flights;
            }
            case "departure" -> {
                for (Flight flight : list) {
                    if (flight.getDeparture().equals(value))
                        flights.add(flight);
                }

                return flights;
            }
            case "date" -> {
                for (Flight flight : list) {
                    if (flight.getDate().equals(LocalDate.parse(value)))
                        flights.add(flight);
                }

                return flights;
            }
            case "flight_number" -> {
                for (Flight flight : list) {
                    if (flight.getFlightNumber().equals(value))
                        flights.add(flight);
                }

                return flights;
            }
            case "plane_unique_id" -> {
                for (Flight flight : list) {
                    if (flight.getPlaneUniqueId().equals(value))
                        flights.add(flight);
                }

                return flights;
            }

            default -> throw new ConflictException("Illegal argument sent");
        }
    }
}
