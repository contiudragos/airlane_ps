package com.airline.service;

import com.airline.exception.ConflictException;
import com.airline.exception.MinimumAgeException;
import com.airline.model.Employee;
import com.airline.repository.BaseRepository;
import com.airline.repository.implementation.EmployeeRepositoryDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private final BaseRepository<Employee> employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepositoryDB employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    /**
     * @return Return a list with all employees
     */
    public List<Object> getAllEmployees() {
        return Collections.singletonList(employeeRepository.getAll());
    }

    /**
     * @param employee Adds a new employee
     */
    public void addEmployee(Employee employee) {
        employeeRepository.add(employee);
    }

    /**
     * @param employee
     * @throws ConflictException
     * @throws MinimumAgeException
     */
    public void validateNewEmployee(Employee employee) throws ConflictException, MinimumAgeException {
        List<Employee> result = employeeRepository.getAll();

        int age = Period.between(employee.getBirthday(), LocalDate.now()).getYears();
        if (age <= 18) {
            throw new MinimumAgeException("You must be at least 18 years old");
        }
    }

    /**
     * @param employee
     * @throws ConflictException
     * @throws MinimumAgeException Checks if there is a user with given id
     *                             Checks if there is the email is unique
     *                             Checks if the user is older than 18
     */
    public void validateUpdateEmployee(Employee employee) throws ConflictException, MinimumAgeException {
        Optional<Employee> result = employeeRepository.findById(employee.getId());

        if (result.isEmpty()) {
            throw new ConflictException("There is no user with this id");
        }

        int age = Period.between(employee.getBirthday(), LocalDate.now()).getYears();
        if (age <= 18) {
            throw new MinimumAgeException("The user must be at least 18 years old");
        }
    }
}
