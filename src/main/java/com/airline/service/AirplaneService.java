package com.airline.service;

import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.airline.repository.BaseRepository;
import com.airline.repository.implementation.AirplaneRepositoryDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class AirplaneService {

    private final BaseRepository<Airplane> airplaneRepository;

    @Autowired
    public AirplaneService(AirplaneRepositoryDB airplaneRepository) {
        this.airplaneRepository = airplaneRepository;
    }

    /**
     * @param airplane Adds a new airplane
     */
    public void add(Airplane airplane) {
        this.airplaneRepository.add(airplane);
    }

    /**
     * @return Return a list with all airplanes
     */
    public List<Airplane> getAll() {
        return airplaneRepository.getAll();
    }

    /**
     * @param id
     * Deletes an airplane by id
     */
    public void delete(Long id) {
        airplaneRepository.delete(id);
    }

    /**
     * @throws ConflictException Checks if the unique id given is already used
     *                           Checks if the production date is in the future
     */
    public void validateNewAirplane(Airplane airplane) throws ConflictException {
        LocalDate currentData = LocalDate.now();
        if (currentData.compareTo(airplane.getProductionDate()) < 0) {
            throw new ConflictException("The production date is in the future");
        }

        List<Airplane> existingAirplane = this.airplaneRepository.findByCriteria("unique_id", airplane.getUniqueId());
        if(existingAirplane.size() > 0){
            throw new ConflictException("The unique id is already used");
        }
    }

    /**
     * @throws ConflictException Checks if we try to put the same id on 2 planes
     *                           Checks if the production date is in the future
     *                           Checks if the unique id is already used
     */
    public void validateUpdateAirplane(Airplane airplane) throws ConflictException {
        Optional<Airplane> result = this.airplaneRepository.findById(airplane.getId());

        if (result.isEmpty()) {
            throw new ConflictException("There is no plane with this id");
        }

        LocalDate currentData = LocalDate.now();
        if (currentData.compareTo(airplane.getProductionDate()) < 0) {
            throw new ConflictException("The production date is in the future");
        }

        List<Airplane> airplanes = this.airplaneRepository.findByCriteria("unique_id", airplane.getUniqueId());
        if(airplanes.size() > 0){
            throw new ConflictException("This unique id is already used");
        }
    }

    /**
     * @throws ConflictException Checks if there is an airplane with the given id
     */
    public void validateDeleteAirplane(Long id) throws ConflictException {
        Optional<Airplane> result = this.airplaneRepository.findById(id);

        if (result.isEmpty()) {
            throw new ConflictException("There is no plane with this id");
        }
    }

    /**
     * @param id
     * @return an airplane with given id
     */
    public Optional<Airplane> findAirplaneById(Long id) {
        return this.airplaneRepository.findById(id);
    }
}
