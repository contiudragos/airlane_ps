package com.airline.service;

import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.airline.model.Airport;
import com.airline.model.Flight;
import com.airline.repository.BaseRepository;
import com.airline.repository.implementation.AirplaneRepositoryDB;
import com.airline.repository.implementation.AirportRepositoryDB;
import com.airline.repository.implementation.FlightRepositoryDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FlightService {

    private final BaseRepository<Flight> flightRepository;
    private final BaseRepository<Airplane> airplaneRepository;
    private final BaseRepository<Airport> airportBaseRepository;

    @Autowired
    public FlightService(FlightRepositoryDB flightRepository, AirplaneRepositoryDB airplaneRepository, AirportRepositoryDB airportBaseRepository) {
        this.flightRepository = flightRepository;
        this.airplaneRepository = airplaneRepository;
        this.airportBaseRepository = airportBaseRepository;
    }

    /**
     * @param flight
     */
    public void addFlight(Flight flight) {
        this.flightRepository.add(flight);
    }

    /**
     * @return
     */
    public List<Flight> getFlights() {
        return this.flightRepository.getAll();
    }

    /**
     * @param id
     */
    public void deleteById(Long id) {
        this.validateDeleteFlight(id);
        this.flightRepository.delete(id);
    }

    /**
     * @param uid
     */
    public List<Flight> findFlightsByPlaneUid(String uid) {
        return this.flightRepository.findByCriteria("plane_unique_id", uid);
    }

    /**
     * @param flight
     * @throws ConflictException
     */
    public void validateNewFlight(Flight flight) throws ConflictException {
        List<Flight> flights = this.flightRepository.findByCriteria("flight_number", flight.getFlightNumber());
        if (flights.size() > 0) {
            throw new ConflictException("There is already a flight with this number");
        }

        List<Airplane> airplanes = this.airplaneRepository.findByCriteria("unique_id", flight.getPlaneUniqueId());
        if (airplanes.size() == 0) {
            throw new ConflictException("There is no plane with this unique id");
        }

        List<Airport> airports = this.airportBaseRepository.findByCriteria("location", flight.getDeparture());
        if (airports.size() == 0) {
            throw new ConflictException("There is no airport in this departure location");
        }

        airports = this.airportBaseRepository.findByCriteria("location", flight.getArrival());
        if (airports.size() == 0) {
            throw new ConflictException("There is no airport in this arrival location");
        }
    }

    /**
     * @param flight Checks if there is a flight with given id
     *               Checks if we try to have 2 planes with the same flight number
     *               Checks if there is an airplane with given unique id
     */
    public void validateUpdateFlight(Flight flight) {
        Optional<Flight> result = this.flightRepository.findById(flight.getId());

        if (result.isEmpty()) {
            throw new ConflictException("There is no flight with this id");
        }

        List<Flight> flights = this.flightRepository.findByCriteria("flight_number", flight.getFlightNumber());
        if (flights.size() > 0) {
            throw new ConflictException("There is already a flight with this number");
        }

        List<Airport> airports = this.airportBaseRepository.findByCriteria("location", flight.getDeparture());
        if (airports.size() == 0) {
            throw new ConflictException("There is no airport in this departure location");
        }

        airports = this.airportBaseRepository.findByCriteria("location", flight.getArrival());
        if (airports.size() == 0) {
            throw new ConflictException("There is no airport in this arrival location");
        }
    }

    public void validateDeleteFlight(Long id) {
        Optional<Flight> result = this.flightRepository.findById(id);

        if (result.isEmpty()) {
            throw new ConflictException("There is no flight with this id");
        }
    }

    public List<Flight> findByCriteria(String argument, String value) {
        return this.flightRepository.findByCriteria(argument, value);
    }
}
