package com.airline.service;

import com.airline.exception.ConflictException;
import com.airline.model.Airport;
import com.airline.model.Flight;
import com.airline.repository.BaseRepository;
import com.airline.repository.implementation.AirportRepositoryDB;
import com.airline.repository.implementation.FlightRepositoryDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirportService {

    private final BaseRepository<Airport> airportRepository;

    @Autowired
    public AirportService(AirportRepositoryDB airportRepository) {
        this.airportRepository = airportRepository;
    }

    /**
     * @return a list with all airports
     */
    public List<Airport> getAirports() {
        return this.airportRepository.getAll();
    }

    /**
     * @param airport Adds a new airport
     */
    public void addAirport(Airport airport) {
        this.validateNewAirport(airport);
        airportRepository.add(airport);
    }

    public void deleteAirport(Long id){
        this.validateDeleteAirport(id);
        airportRepository.delete(id);
    }

    /**
     * @param airport
     * @throws ConflictException Check if the name is already used
     */
    public void validateNewAirport(Airport airport) throws ConflictException {
        List<Airport> airports = this.airportRepository.findByCriteria("name", airport.getName());
        if(airports.size() > 0){
            throw new ConflictException("There is already an airport with this name");
        }
    }

    /**
     * @throws ConflictException Checks if there is an existing airport with given id
     */
    public void validateUpdateAirport(Airport airport) throws ConflictException {
        Optional<Airport> result = airportRepository.findById(airport.getId());

        if (result.isEmpty()) {
            throw new ConflictException("There is no airport with this id");
        }
    }

    /**
     * @throws ConflictException Checks if there is an airport with given id
     */
    public void validateDeleteAirport(Long id) throws ConflictException{
        Optional<Airport> result = airportRepository.findById(id);

        if (result.isEmpty()) {
            throw new ConflictException("There is no airport with this id");
        }
    }

    public Optional<Airport> findById(Long id) {
        return this.airportRepository.findById(id);
    }
}
