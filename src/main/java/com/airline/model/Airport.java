package com.airline.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "airport", schema = "public")
@Getter
@Setter
public class Airport {
    @Id
    @SequenceGenerator(
            name = "airport_sequence",
            sequenceName = "airport_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "airport_sequence"
    )
    private Long id;
    private String name;
    private String location;
    private int isAvailable;
    private int hasParking;

    public Airport() {
    }
}
