package com.airline.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "flight", schema = "public")
public class Flight {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(
            name = "flight_sequence",
            sequenceName = "flight_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "flight_sequence"
    )
    private Long id;
    private String flightNumber;
    private LocalDate date;
    private String departure;
    private String arrival;
    private String planeUniqueId;

    public Flight() {
    }

}
