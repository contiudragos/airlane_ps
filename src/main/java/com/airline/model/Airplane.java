package com.airline.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "airplane", schema = "public")
public class Airplane {
    @Id
    @SequenceGenerator(
            name = "airplane_sequence",
            sequenceName = "airplane_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "airplane_sequence"
    )
    private Long id;

    private String model;
    private LocalDate productionDate;
    private int capacity;
    private String uniqueId;

    public Airplane() {
    }
}
