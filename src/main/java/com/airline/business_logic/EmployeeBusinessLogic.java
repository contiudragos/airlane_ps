package com.airline.business_logic;

import com.airline.model.Employee;
import com.airline.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EmployeeBusinessLogic {

    private final EmployeeService service;

    @Autowired
    public EmployeeBusinessLogic(EmployeeService service) {
        this.service = service;
    }

    /**
     * @return a list with all employees
     */
    public List<Object> getAllEmployees() {
        return this.service.getAllEmployees();
    }

    /**
     * @param employee Validates the data
     *                 Adds a new employee
     */
    public void addEmployee(Employee employee) {
        this.service.validateNewEmployee(employee);
        this.service.addEmployee(employee);
    }

    /**
     * @param employee Validates the data
     *                 Updates an existing employee
     */
    public void updateEmployee(Employee employee) {
        this.service.validateUpdateEmployee(employee);
        this.service.addEmployee(employee);
    }
}
