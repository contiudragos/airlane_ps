package com.airline.business_logic;

import com.airline.model.Airplane;
import com.airline.model.Flight;
import com.airline.service.AirplaneService;
import com.airline.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.List;
import java.util.Optional;

@Controller
public class AirplaneBusinessLogic {
    private final AirplaneService airplaneService;
    private final FlightService flightService;

    @Autowired
    public AirplaneBusinessLogic(AirplaneService service, FlightService flightService) {
        this.airplaneService = service;
        this.flightService = flightService;
    }

    /**
     * @return a list with all airplanes from service
     */
    public List<Airplane> getAirplanes() {
        return this.airplaneService.getAll();
    }

    /**
     * @param airplane Validates the data
     *                 Calls the service to add a new airplane
     */
    public void addAirplane(Airplane airplane) {
        airplaneService.validateNewAirplane(airplane);
        airplaneService.add(airplane);
    }

    /**
     * @param airplane Validates the data
     *                 Calls the service to update an existing airplane
     */
    public void updateAirplane(Airplane airplane) {
        airplaneService.validateUpdateAirplane(airplane);
        airplaneService.add(airplane);
    }

    /**
     * @param id The method find the airplane with the given id
     *           Validates the input
     *           Calls the service to delete all the corresponding flights
     *           Calls the service to delete the airplane
     */
    public void deleteAirplane(Long id) {
        Optional<Airplane> airplane = airplaneService.findAirplaneById(id);
        airplaneService.validateDeleteAirplane(id);

        airplane.ifPresent(plane -> {
            List<Flight> flights = flightService.findFlightsByPlaneUid((plane).getUniqueId());

            if (flights.size() > 0) {
                for(Flight flight: flights) {
                    flightService.deleteById(flight.getId());
                }
            }
        });

        airplaneService.delete(id);
    }
}
