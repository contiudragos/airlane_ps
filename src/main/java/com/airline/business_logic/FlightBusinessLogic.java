package com.airline.business_logic;

import com.airline.model.Flight;
import com.airline.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class FlightBusinessLogic {

    private final FlightService service;

    @Autowired
    public FlightBusinessLogic(FlightService service) {
        this.service = service;
    }

    /**
     * @return a list with all flights
     */
    public List<Flight> getFlights() {
        return service.getFlights();
    }

    /**
     * @param flight Validates the data
     *               Adds a new flight
     */
    public void addFlight(Flight flight) {
        this.service.validateNewFlight(flight);
        this.service.addFlight(flight);
    }

    /**
     * @param flight Validates the data
     *               Updates an existing flight
     */
    public void updateFlight(Flight flight) {
        this.service.validateUpdateFlight(flight);
        this.service.addFlight(flight);
    }

    public void deleteFlight(Long id) {
        this.service.validateDeleteFlight(id);
        this.service.deleteById(id);
    }
}
