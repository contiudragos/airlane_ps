package com.airline.business_logic;

import com.airline.model.Airport;
import com.airline.model.Flight;
import com.airline.service.AirportService;
import com.airline.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;

@Controller
public class AirportBusinessLogic {

    private final AirportService service;
    private final FlightService flightService;

    @Autowired
    public AirportBusinessLogic(AirportService service, FlightService flightService) {
        this.service = service;
        this.flightService = flightService;
    }

    /**
     * @return a list with all Airports
     */
    public List<Airport> getAirports() {
        return this.service.getAirports();
    }

    /**
     * @param airport Validates the data
     *                Adds a new Airport
     */
    public void addAirport(Airport airport) {
        this.service.validateNewAirport(airport);
        this.service.addAirport(airport);
    }

    /**
     * @param airport Validates the data
     *                Updates an existing airport
     */
    public void updateAirport(Airport airport) {
        this.service.validateUpdateAirport(airport);
        this.service.addAirport(airport);
    }

    /**
     * @param id Validates the data
     *           Deletes if everything is validated
     */
    public void deleteAirport(Long id){
        this.service.validateDeleteAirport(id);

        Optional<Airport> airport = this.service.findById(id);
        airport.ifPresent(
                airportFound -> {
                    List<Flight> flights = this.flightService.findByCriteria("arrival", airportFound.getLocation());
                    for(Flight flight: flights){
                        this.flightService.deleteById(flight.getId());
                    }

                    flights = this.flightService.findByCriteria("departure", airportFound.getLocation());
                    for(Flight flight: flights){
                        this.flightService.deleteById(flight.getId());
                    }
                }
        );
        this.service.deleteAirport(id);
    }
}
