package laborator;

public class FactoryUser {

    public User createUser(Roles role, String firstName, String lastName){
        return switch (role) {
            case CONTRACTOR -> new Contractor(firstName, lastName);
            case EMPLOYEE -> new Employee(firstName, lastName);
            case VISITOR -> new Visitor(firstName, lastName);
        };
    }
}
