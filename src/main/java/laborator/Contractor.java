package laborator;

public class Contractor extends User {

    public Contractor(String firstName, String lastName){
        super(firstName,lastName);
    }
    @Override
    public boolean equals(Object obj) {
        return ((Contractor)obj).firstName.equals(this.firstName) && ((Contractor)obj).lastName.equals(this.lastName);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
