package laborator;

public class Employee extends User {

    public Employee(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public boolean equals(Object obj) {
        return ((Employee)obj).firstName.equals(this.firstName) && ((Employee)obj).lastName.equals(this.lastName);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
