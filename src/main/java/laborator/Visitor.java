package laborator;

public class Visitor extends User {

    public Visitor(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public boolean equals(Object obj) {
        return ((Visitor)obj).firstName.equals(this.firstName) && ((Visitor)obj).lastName.equals(this.lastName);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
