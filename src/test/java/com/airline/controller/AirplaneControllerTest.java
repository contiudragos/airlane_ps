package com.airline.controller;

import com.airline.business_logic.AirplaneBusinessLogic;
import com.airline.data.AirplaneData;
import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AirplaneController.class)
class AirplaneControllerTest {

    @MockBean
    private AirplaneBusinessLogic airplaneBusinessLogic;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAirplanesSuccess() throws Exception {
        List<Airplane> expectedResult = AirplaneData.createGetAllExpectedSuccessResponse();

        Mockito.when(airplaneBusinessLogic.getAirplanes()).thenReturn(expectedResult);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/airplane"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(11)))
                .andExpect(jsonPath("$[0].model", is("Boeing 777")))
                .andExpect(jsonPath("$[0].uniqueId", is("WZ-1257")))
                .andExpect(jsonPath("$[1].id", is(12)))
                .andExpect(jsonPath("$[1].model", is("Boeing 777")))
                .andExpect(jsonPath("$[1].uniqueId", is("RS-3247")));
    }

    @Test
    void addAirplaneSuccess() throws Exception {
        Airplane airplane = AirplaneData.createAirplaneModelSuccess();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsBytes(airplane)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Airplane successfully added")));
    }

    @Test
    void addAirplaneThrowException() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsBytes("airplane")))
                .andExpect(status().isBadRequest());

        Airplane airplane = AirplaneData.createAirplaneModelThrowException();
        doThrow(ConflictException.class)
                .when(airplaneBusinessLogic)
                .addAirplane(airplane);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(String.valueOf(airplane)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateAirplaneSuccess() throws Exception {
        Airplane airplane = AirplaneData.createUpdateAirplaneModelSuccess();

        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsBytes(airplane)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Airplane successfully updated")));
    }

    @Test
    void updateAirplaneThrowException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsBytes("airplane")))
                .andExpect(status().isBadRequest());

        Airplane airplane = AirplaneData.createAirplaneModelThrowException();
        doThrow(ConflictException.class)
                .when(airplaneBusinessLogic)
                .updateAirplane(airplane);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(String.valueOf(airplane)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteAirplaneSuccess() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/airplane/11")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Airplane successfully deleted")));
    }

    @Test
    void deleteAirplaneThrowException() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsBytes("airplane")))
                .andExpect(status().isBadRequest());

        doThrow(ConflictException.class)
                .when(airplaneBusinessLogic)
                .deleteAirplane(11L);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/airplane/11")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }
}