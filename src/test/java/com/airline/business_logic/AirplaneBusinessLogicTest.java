package com.airline.business_logic;

import com.airline.data.AirplaneData;
import com.airline.exception.ConflictException;
import com.airline.model.Airplane;
import com.airline.service.AirplaneService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
class AirplaneBusinessLogicTest {

    @InjectMocks
    private AirplaneBusinessLogic airplaneBusinessLogic;

    @Mock
    private AirplaneService airplaneService;

    @Test
    void testGetAll() {
        List<Airplane> expectedResult = AirplaneData.createGetAllExpectedSuccessResponse();
        when(airplaneService.getAll()).thenReturn(expectedResult);

        Assertions.assertEquals(expectedResult, airplaneBusinessLogic.getAirplanes());
        Assertions.assertEquals(airplaneBusinessLogic.getAirplanes().size(), 2);
    }

    @Test
    void testAddSuccess() {
        Airplane airplane = AirplaneData.createAirplaneModelSuccess();
        doNothing()
                .when(airplaneService)
                .add(airplane);

        airplaneBusinessLogic.addAirplane(airplane);
    }

    @Test
    void testDeleteSuccess() {
        doNothing()
                .when(airplaneService)
                .delete(11L);

        airplaneBusinessLogic.deleteAirplane(11L);
    }

    @Test
    void testValidateDeleteAirplane() {
        doThrow(new ConflictException("There is no plane with this id"))
                .when(airplaneService)
                .validateDeleteAirplane(96L);

        doThrow(new ConflictException("There is no plane with this id"))
                .when(airplaneService)
                .delete(96L);

        ConflictException exception = Assertions.assertThrows(ConflictException.class, () -> {
            airplaneBusinessLogic.deleteAirplane(96L);
        }, "Exception was expected");

        Assertions.assertEquals("There is no plane with this id", exception.getMessage());
    }

    @Test
    void testValidateAddAirplane(){
        Airplane airplane = AirplaneData.createAirplaneModelThrowException();
        doThrow(new ConflictException("There is no plane with this id"))
                .when(airplaneService)
                .validateNewAirplane(airplane);

        doThrow(new ConflictException("There is no plane with this id"))
                .when(airplaneService)
                .add(airplane);

        ConflictException exception = Assertions.assertThrows(ConflictException.class, () -> {
            airplaneBusinessLogic.addAirplane(airplane);
        }, "Exception was expected");

        Assertions.assertEquals("There is no plane with this id", exception.getMessage());
    }
}
