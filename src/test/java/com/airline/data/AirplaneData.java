package com.airline.data;

import com.airline.model.Airplane;
import org.mockito.stubbing.Answer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AirplaneData {

    public static List<Airplane> createGetAllExpectedSuccessResponse() {
        ArrayList<Airplane> data = new ArrayList<>();
        Airplane a1 = new Airplane();

        a1.setId(11L);
        a1.setCapacity(300);
        a1.setModel("Boeing 777");
        a1.setProductionDate(LocalDate.parse("2018-04-10"));
        a1.setUniqueId("WZ-1257");

        Airplane a2 = new Airplane();
        a2.setId(12L);
        a2.setCapacity(300);
        a2.setModel("Boeing 777");
        a2.setProductionDate(LocalDate.parse("2018-04-10"));
        a2.setUniqueId("RS-3247");

        data.add(a1);
        data.add(a2);

        return data;
    }

    public static Airplane createAirplaneModelSuccess() {
        Airplane a = new Airplane();
        a.setModel("Airbus 111");
        a.setCapacity(200);
        a.setUniqueId("ZS-1478");
        a.setProductionDate(LocalDate.parse("2022-01-01"));

        return a;
    }

    public static Airplane createAirplaneModelThrowException() {
        Airplane a = new Airplane();
        a.setId(55L);
        a.setModel("Airbus 111");
        a.setCapacity(200);
        a.setUniqueId("WZ-1257");
        a.setProductionDate(LocalDate.parse("2032-01-01"));

        return a;
    }

    public static Airplane createUpdateAirplaneModelSuccess() {
        Airplane a = new Airplane();
        a.setId(11L);
        a.setModel("Airbus 111");
        a.setCapacity(200);
        a.setUniqueId("WZ-1251");
        a.setProductionDate(LocalDate.parse("2012-01-01"));

        return a;
    }
}
