# Airlane Company
### The main purpose of the project

The main purpose for this project was to learn basic REST API in Spring Boot, feeling more comfortable working with java
and MYSQL/Postgres. Also, Postman was a grate tool to use, and it's for sure a good practice for the future. Unit tests were a
big part of the project because they gave me the worst nightmares. :)

### Endpoints that you can access via Postman
- http://localhost:8080/api/v1/airplane
- http://localhost:8080/api/v1/airport
- http://localhost:8080/api/v1/employee
- http://localhost:8080/api/v1/flight

All endpoints have 4 methods implemented: GET, POST, PUT, DELETE

- GET - gives you all the entities from the database
- POST - inserts an entity if all the details given are correct and a status of 200 or 400 if something went wrong
- PUT - updates an entity if all the details given are correct and a status of 200 or 400 if something went wrong
- DELETE - deletes an entity if all the details given are correct and a status of 200 or 400 if something went wrong

![flow_diagram](flow.png)

So basically, the user access the endpoint in API which is found in Controller classes all around the project.
Then, it's called the logic of the project which consists of BusinessLogic classes, where with the help of the Service classes
the data is validated and processed. Then it's sent to the Repository which can be directly working with the database
or could work with some local arrays that allows you to work even if thw database server is down. Then, the data 
is stored and the user receive a response so everybody is happy.

### Good thing to know
For all insert, update and delete methods that are implemented, there are validations in order to keep the data
as clean as possible. If you get an 400 with a custom message, ypu probably entered some malicious data.


### Database diagram
![database_diagram](DB.png)
Maybe if you didn't understand how classes are connected, this will be helpful for you.


